package org.firstinspires.ftc.teamcode.robot;


import com.qualcomm.hardware.motors.GoBILDA5201Series;
import com.qualcomm.hardware.motors.GoBILDA5202Series;
import com.qualcomm.hardware.motors.RevRobotics20HdHexMotor;
import com.qualcomm.robotcore.hardware.configuration.typecontainers.MotorConfigurationType;

public class Constants {

    /*
     * The type of motor used on the drivetrain. While the SDK has definitions for many common
     * motors, there may be slight gear ratio inaccuracies for planetary gearboxes and other
     * discrepancies. Additional motor types can be defined via an interface with the
     * @DeviceProperties and @MotorType annotations.
     */
    private static final MotorConfigurationType MOTOR_CONFIG =
            MotorConfigurationType.getMotorType(RevRobotics20HdHexMotor.class);

    //5202-0002-0014
//    private static final MotorConfigurationType MOTOR_CONFIG =
//            MotorConfigurationType.getMotorType(GoBILDA5202Series.class);

    /*
     * Set the first flag appropriately. If using the built-in motor velocity PID, update
     * MOTOR_VELO_PID with the tuned coefficients from DriveVelocityPIDTuner.
     */
    public static final boolean RUN_USING_ENCODER = true;

//    public static double ROBOT_LENGTH = 39.5;
//    public static double ROBOT_WIDTH = 31;

    public static double ROBOT_LENGTH = 33.5;
    public static double ROBOT_WIDTH = 41;

    public static double ROBOT_C_LENGTH = 30.75;
    public static double ROBOT_C_WIDTH = 39;
    public static double X_TRK_WHL_Y_OFFSET = 14.5;

    public static double X_TRK_WHL_Y_OFFSET_RBT_D = 0.5;
    public static double ROBOT_D_LENGTH = 33;
    public static double ROBOT_D_WIDTH = 40;

    /*
     * These are physical constants that can be determined from your robot (including the track
     * width; it will be tune empirically later although a rough estimate is important). Users are
     * free to chose whichever linear distance unit they would like so long as it is consistently
     * used. The default values were selected with inches in mind. Road runner uses radians for
     * angular distances although most angular parameters are wrapped in Math.toRadians() for
     * convenience. Make sure to exclude any gear ratio included in MOTOR_CONFIG from GEAR_RATIO.
     */
//    public static double WHEEL_DIAMETER = 7.62;
    public static double WHEEL_DIAMETER = 7.87;
    public static double GEAR_RATIO = 0.5;
    public static double PLANETARY_GEAR_RATIO = 13.7; // GOBILDA 5202-0002-0014
    public static double ENCODER_RES_PER_REV = 29; // GOBILDA 5202-0002-0014
//    public static double GEAR_RATIO = 1; // output (wheel) speed / input (motor) speed
    public static double TRACK_WIDTH = 39.5;

    /*
     * These values are used to generate the trajectories for you robot. To ensure proper operation,
     * the constraints should never exceed ~80% of the robot's actual capabilities. While Road
     * Runner is designed to enable faster autonomous motion, it is a good idea for testing to start
     * small and gradually increase them later after everything is working. The velocity and
     * acceleration values are required, and the jerk values are optional (setting a jerk of 0.0
     * forces acceleration-limited profiling).
     */
    public static double getTicksPerRev () {
        return MOTOR_CONFIG.getTicksPerRev();
    }

    public static double encoderTicksToMeters(double ticks) {
        return 7.62 * Math.PI * 1 * ticks / (28 * 20);//MOTOR_CONFIG.getTicksPerRev();
        //return WHEEL_DIAMETER * Math.PI * GEAR_RATIO * ticks / (ENCODER_RES_PER_REV * PLANETARY_GEAR_RATIO);//MOTOR_CONFIG.getTicksPerRev();
    }

    public static double rpmToVelocity(double rpm) {
        return rpm * GEAR_RATIO * 2 * Math.PI * WHEEL_DIAMETER / 60.0;
    }

    public static double getMaxRpm() {
        return MOTOR_CONFIG.getMaxRPM() *
                (RUN_USING_ENCODER ? MOTOR_CONFIG.getAchieveableMaxRPMFraction() : 1.0);
    }

    public static double getMaxSpeedPerSec () {
        //MOTOR_CONFIG.getMaxRPM()
//        return 435 / 60 * WHEEL_DIAMETER * Math.PI * GEAR_RATIO;
        return 435 * WHEEL_DIAMETER * Math.PI / 13.7 / 2 / 60;
    }

    public static double getTicksPerSec() {
        // note: MotorConfigurationType#getAchieveableMaxTicksPerSecond() isn't quite what we want
        return (MOTOR_CONFIG.getMaxRPM() * MOTOR_CONFIG.getTicksPerRev() / 60.0);
    }

    public static double getMotorVelocityF() {
        // see https://docs.google.com/document/d/1tyWrXDfMidwYyP_5H4mZyVgaEswhOC35gvdmP-V-5hA/edit#heading=h.61g9ixenznbx
        return 32767 / getTicksPerSec();
    }
}
