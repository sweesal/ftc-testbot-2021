package org.firstinspires.ftc.teamcode.lib.debugPacket;

public class PathConfig {

    public double maxVel;
    public double maxAcc;
    public double maxAngVel;
    public double spacing;
    public double followDistance;
    public double targetTolerance;
    public double kS;
    public double kV;
    public double kA;

    public PathConfig () {

    }

    public static PathConfig applyPathConfig () {
        return new PathConfig();
    }


}
