package org.firstinspires.ftc.teamcode.lib.util;

public interface CSVWritable {
    String toCSV();
}
