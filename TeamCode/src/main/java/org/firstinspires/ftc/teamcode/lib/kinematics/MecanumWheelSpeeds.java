package org.firstinspires.ftc.teamcode.lib.kinematics;

public class MecanumWheelSpeeds {

    public double leftFrontMetersPerSecond;
    public double leftRearMetersPerSecond;
    public double rightFrontMetersPerSecond;
    public double rightRearMetersPerSecond;

    public MecanumWheelSpeeds(
            double rightFrontMetersPerSecond,
            double leftFrontMetersPerSecond,
            double leftRearMetersPerSecond,
            double rightRearMetersPerSecond) {
        this.rightFrontMetersPerSecond = rightFrontMetersPerSecond;
        this.leftFrontMetersPerSecond = leftFrontMetersPerSecond;
        this.leftRearMetersPerSecond = leftRearMetersPerSecond;
        this.rightRearMetersPerSecond = rightRearMetersPerSecond;
    }

}
