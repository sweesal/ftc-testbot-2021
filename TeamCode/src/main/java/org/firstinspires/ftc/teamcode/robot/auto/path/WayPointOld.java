package org.firstinspires.ftc.teamcode.robot.auto.path;

public class WayPointOld {
    public double x;
    public double y;
    public double moveSpeed;
    public double turnSpeed;
    public double followDistance;
    public double pointLength;
    public double slowDownTurnRadius;
    public double slowDownTurnAmount;


    public WayPointOld(double x, double y, double moveSpeed, double turnSpeed, double followDistance,
                       double slowDownTurnRadius, double slowDownTurnAmount){
        this.x = x;
        this.y = y;
        this.moveSpeed = moveSpeed;
        this.turnSpeed = turnSpeed;
        this.followDistance = followDistance;
        this.slowDownTurnRadius = slowDownTurnRadius;
        this.slowDownTurnAmount = slowDownTurnAmount;
    }

    public WayPointOld(WayPointOld thisPoint){
        x = thisPoint.x;
        y = thisPoint.y;
        moveSpeed = thisPoint.moveSpeed;
        turnSpeed = thisPoint.turnSpeed;
        followDistance = thisPoint.followDistance;
        slowDownTurnAmount = thisPoint.slowDownTurnAmount;
        slowDownTurnRadius = thisPoint.slowDownTurnRadius;
        pointLength = thisPoint.pointLength;
    }

    public Point toPoint(){
        return new Point(x, y);
    }

    public void setPoint(Point point){
        x = point.x;
        y = point.y;
    }

}
