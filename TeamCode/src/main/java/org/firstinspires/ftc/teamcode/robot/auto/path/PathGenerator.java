package org.firstinspires.ftc.teamcode.robot.auto.path;

import org.firstinspires.ftc.teamcode.robot.auto.config.*;
import java.util.ArrayList;

public class PathGenerator {

    public PathGenerator () {

    }

    public ArrayList<Waypoint> generate (ArrayList<Waypoint> nodes, PathConfig config) {
        int injectNum = nodes.size() * 10;
        ArrayList<Waypoint> path = new ArrayList<Waypoint>();
        path = injectMorePoints(nodes, injectNum);
        path = smoothPathPoints(path, 0.7, 0.3, 0.01);
        injectVelocities(path, config);
        return path;
    }

    public ArrayList<Waypoint> injectMorePoints(ArrayList<Waypoint> path, int numToInject) {
        double[][] orig = waypointsToArray(path);
        double[][] morePoints;
        morePoints = new double[orig.length + ((numToInject) * (orig.length - 1))][2];
        int index = 0;

        for (int i = 0; i < orig.length - 1; i++) {
            morePoints[index][0] = orig[i][0];
            morePoints[index][1] = orig[i][1];
            index++;
            for (int j = 1; j < numToInject + 1; j++) {
                morePoints[index][0] = j * ((orig[i + 1][0] - orig[i][0]) / (numToInject + 1)) + orig[i][0];
                morePoints[index][1] = j * ((orig[i + 1][1] - orig[i][1]) / (numToInject + 1)) + orig[i][1];
                index++;
            }
        }
        morePoints[index][0] = orig[orig.length - 1][0];
        morePoints[index][1] = orig[orig.length - 1][1];
        return arrayToWaypoints(morePoints);
    }


    /**
     * Team 2164's algorithm.
     *
     * Optimization algorithm, which optimizes the data points in path to create a smooth trajectory.
     * This optimization uses gradient descent. While unlikely, it is possible for this algorithm to never
     * converge. If this happens, try increasing the tolerance level.
     *
     * BigO: N^x, where X is the number of of times the while loop iterates before tolerance is met.
     *
     * @param inputPath
     * @param weight_data
     * @param weight_smooth
     * @param tolerance
     * @return
     */
    public ArrayList<Waypoint> smoothPathPoints(ArrayList<Waypoint> inputPath, double weight_data, double weight_smooth, double tolerance)
    {
        double[][] path = waypointsToArray(inputPath);
        double[][] newPath = waypointsToArray(inputPath);

        double change = tolerance;
        while (change >= tolerance) {
            change = 0.0;
            for (int i = 1; i < path.length - 1; i++)
                for (int j = 0; j < path[i].length; j++) {
                    double aux = newPath[i][j];
                    newPath[i][j] += weight_data * (path[i][j] - newPath[i][j])
                            + weight_smooth * (newPath[i - 1][j] + newPath[i + 1][j] - (2.0 * newPath[i][j]));
                    change += Math.abs(aux - newPath[i][j]);
                }
        }
        return arrayToWaypoints(newPath);
    }

    // See https://www.qc.edu.hk/math/Advanced%20Level/circle%20given%203%20points.htm to get curvature equation.
    public void injectVelocities (ArrayList<Waypoint> path, PathConfig config) {
        ArrayList<Waypoint> waypoints = path;

        for (int i = 0; i < waypoints.size(); i++) {
            double curvature = 0;
            if (i != 0 && i != waypoints.size() - 1) {
                double x1 = waypoints.get(i).x + 0.0001, y1 = waypoints.get(i).y + 0.0001;
                double x2 = waypoints.get(i - 1).x, y2 = waypoints.get(i - 1).y;
                double x3 = waypoints.get(i + 1).x, y3 = waypoints.get(i + 1).y;

                double k1 = 0.5 * (x1 * x1 + y1 * y1 - x2 * x2 - y2 * y2) / (x1 - x2);
                double k2 = (y1 - y2) / (x1 - x2);
                double b = 0.5 * (x2 * x2 - 2 * x2 * k1 + y2 * y2 - x3 * x3 + 2 * x3 * k1 - y3 * y3)
                        / (x3 * k2 - y3 + y2 - x2 * k2);
                double a = k1 - k2 * b;
                double r = Math.sqrt((x1 - a) * (x1 - a) + (y1 - b) * (y1 - b));
                curvature = 1 / r;
            }
            waypoints.get(i).velocity = Math.min(config.maxVel, config.maxAngVel / curvature);
        }

        waypoints.get(waypoints.size() - 1).velocity = 0;

        for (int i = waypoints.size() - 1 - 1; i >= 0; i--) {
            double distance = distanceBetween(waypoints.get(i + 1), waypoints.get(i));

            waypoints.get(i).velocity = Math.min(waypoints.get(i).velocity,
                    getMaxAchieveableVelocity(config.maxAcc, distance, waypoints.get(i + 1).velocity));
        }
    }

    private double getMaxAchieveableVelocity(double timeAccel, double distance, double velocity0) {
        return Math.sqrt(velocity0 * velocity0 + 2 * timeAccel * distance);
    }

    public double[][] waypointsToArray (ArrayList<Waypoint> waypointOrigs) {
        double[][] arr = new double[waypointOrigs.size()][2];
        for (int i = 0; i < waypointOrigs.size(); i++) {
            arr[i][0] = waypointOrigs.get(i).x;
            arr[i][1] = waypointOrigs.get(i).y;
        }
        return arr;
    }

    public ArrayList<Waypoint> arrayToWaypoints(double[][] array) {
        ArrayList<Waypoint> waypointOrigin = new ArrayList<Waypoint>();
        for (double[] doubles : array) {
            waypointOrigin.add(new Waypoint(doubles[0], doubles[1], 0));
        }
        return waypointOrigin;
    }

    private double distanceBetween(Waypoint a, Waypoint b) {
        return Math.sqrt(Math.pow(b.x - a.x, 2) + Math.pow(b.y - a.y, 2));
    }

    public double getPathDistance(ArrayList<Waypoint> path) {
        double distance = 0;
        for (int i = 0; i < path.size() - 1; i++) {
            distance += distanceBetween(path.get(i), path.get(i + 1));
        }
        return distance;
    }


}
