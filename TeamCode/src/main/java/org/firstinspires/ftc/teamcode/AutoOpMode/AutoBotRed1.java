package org.firstinspires.ftc.teamcode.AutoOpMode;

import com.acmerobotics.roadrunner.geometry.Pose2d;
import com.acmerobotics.roadrunner.geometry.Vector2d;
import com.acmerobotics.roadrunner.trajectory.Trajectory;
import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.util.ElapsedTime;

import org.firstinspires.ftc.teamcode.drive.SampleMecanumDrive;
import org.firstinspires.ftc.teamcode.robot.RobotMap;
import org.firstinspires.ftc.teamcode.robot.subsystems.Intake;
import org.firstinspires.ftc.teamcode.robot.subsystems.Shooter;
import org.firstinspires.ftc.teamcode.robot.subsystems.WobbleGoalGrabber;


@Autonomous(name="AutoRed1", group="Auto")
//@Disabled
public class AutoBotRed1 extends LinearOpMode {

    private final ElapsedTime runtime = new ElapsedTime();
    private Intake intake;
    private Shooter shooter;
    private WobbleGoalGrabber wobbleGoalGrabber;

    private RobotMap robotMap = new RobotMap();

    private static int pathNum = 4;

    @Override
    public void runOpMode() {
        robotMap.robotInit(hardwareMap);
        SampleMecanumDrive drive = new SampleMecanumDrive(hardwareMap);

        intake = new Intake();
        shooter = new Shooter();
        Shooter.setIsShooting(false);
        wobbleGoalGrabber = new WobbleGoalGrabber();

        //drive.setPoseEstimate(new Pose2d(0, 0, Math.PI));

        Trajectory trajectory1 = drive.trajectoryBuilder(new Pose2d())
                .splineTo(new Vector2d(75, -5), 0)
                .addDisplacementMarker(60, () -> {
                    // This marker runs 20 inches into the trajectory
                    //shooter.setShooter(true);
                    // Run your action in here!
                })
                .build();


        waitForStart();
        sleep(1000);
        drive.followTrajectory(trajectory1);

    }

    public void trigger () {
        shooter.setTrigger(true);
        sleep(500);
        shooter.setTrigger(false);
        sleep(250);
    }
}
