package org.firstinspires.ftc.teamcode.lib.kinematics;


import org.ejml.simple.SimpleMatrix;

public class TrackingWheelKinematics {
    private SimpleMatrix fwdKinematics;
    private SimpleMatrix invKinematics;
    private SimpleMatrix trackingWheelMatrix = new SimpleMatrix(3, 1); // 3*1 matrix
    private SimpleMatrix chassisSpeedMatrix = new SimpleMatrix(3, 1); // 3*1 matrix

    public TrackingWheelKinematics() {}

    public TrackingWheelKinematics(double xAxisTrackWidth, double yAxisOffset) {
        invKinematics = new SimpleMatrix(3, 3);
        setInvKinematics(xAxisTrackWidth/2, yAxisOffset); // make values half
        fwdKinematics = invKinematics.pseudoInverse();
    }

    private void setFwdKinematics (double xOffset, double yOffset) {
        fwdKinematics.setRow(0, 0, 0.5, 0, 0.5);
        fwdKinematics.setRow(1, 0, 0,  1, 0);
        fwdKinematics.setRow(
                2, 0, - 1 / (2 * xOffset), 0, 1 / (2 * xOffset));
                //2, 0, - 1 / (3 * xOffset), 1 / (3 * yOffset), 1 / (3 * xOffset));
    }

    private void setInvKinematics (double xOffset, double yOffset) {
        invKinematics.setRow(0, 0, 1, 0, -xOffset);
        invKinematics.setRow(1, 0, 0, 1, yOffset);
        invKinematics.setRow(2, 0, 1, 0, xOffset);
    }

    public ChassisSpeeds toChassisSpeed (TrackingWheelSpeeds trackingWheelSpeeds){
        trackingWheelMatrix.setColumn(0, 0,
                trackingWheelSpeeds.leftMetersPerSecond, trackingWheelSpeeds.horizontalMetersPerSecond, trackingWheelSpeeds.rightMetersPerSecond); // Anticlockwise.
        chassisSpeedMatrix = fwdKinematics.mult(trackingWheelMatrix);
        return new ChassisSpeeds(
                chassisSpeedMatrix.get(0, 0),
                chassisSpeedMatrix.get(1, 0),
                chassisSpeedMatrix.get(2, 0)
        );
    }

}
