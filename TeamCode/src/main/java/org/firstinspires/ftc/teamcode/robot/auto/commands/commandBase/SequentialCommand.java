package org.firstinspires.ftc.teamcode.robot.auto.commands.commandBase;

import java.util.ArrayList;
import java.util.List;

public class SequentialCommand implements ICommand {
    private ICommand mCommands;
    private final ArrayList<ICommand> mRemainingCommands;

    public SequentialCommand (List<ICommand> commands) {
        mRemainingCommands = new ArrayList<>(commands);
        mCommands = null;
    }

    @Override
    public void init() {

    }

    @Override
    public void execute() {
        if (mCommands == null) {
            if (mRemainingCommands.isEmpty()) {
                return;
            }

            mCommands = mRemainingCommands.remove(0);
            mCommands.init();
        }

        mCommands.execute();

        if (mCommands.isDone()) {
            mCommands.end();
            mCommands = null;
        }
    }

    @Override
    public void end() {

    }

    @Override
    public boolean isDone() {
        return mRemainingCommands.isEmpty() && mCommands == null;
    }

}
