/* Copyright (c) 2017 FIRST. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted (subject to the limitations in the disclaimer below) provided that
 * the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice, this list
 * of conditions and the following disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice, this
 * list of conditions and the following disclaimer in the documentation and/or
 * other materials provided with the distribution.
 *
 * Neither the name of FIRST nor the names of its contributors may be used to endorse or
 * promote products derived from this software without specific prior written permission.
 *
 * NO EXPRESS OR IMPLIED LICENSES TO ANY PARTY'S PATENT RIGHTS ARE GRANTED BY THIS
 * LICENSE. THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.util.ElapsedTime;

import org.firstinspires.ftc.teamcode.lib.util.ButtonPress;
import org.firstinspires.ftc.teamcode.robot.RobotMap;
import org.firstinspires.ftc.teamcode.robot.auto.AutoTest;
import org.firstinspires.ftc.teamcode.robot.subsystems.DriveTrain;
import org.firstinspires.ftc.teamcode.robot.subsystems.Intake;
import org.firstinspires.ftc.teamcode.robot.subsystems.Shooter;
import org.firstinspires.ftc.teamcode.robot.subsystems.WobbleGoalGrabber;

@com.qualcomm.robotcore.eventloop.opmode.TeleOp(name="单人模式", group="Manual")
//@Disabled

public class OneDriverBot extends OpMode {

    private ElapsedTime runtime = new ElapsedTime();
    private DriveTrain driveTrain;
    private AutoTest auto;
    private Intake intake;
    private Shooter shooter;
    private WobbleGoalGrabber wobbleGoalGrabber;

    private RobotMap robotMap = new RobotMap();

    @Override
    public void init() {

        robotMap.robotInit(hardwareMap);
        driveTrain = DriveTrain.getInstance();
        driveTrain.initMotors();
        auto = AutoTest.getInstance();

        intake = new Intake();
        shooter = new Shooter();
        Shooter.setIsShooting(false);
        wobbleGoalGrabber = new WobbleGoalGrabber();

        telemetry.addData("ringArm", wobbleGoalGrabber.getArmPosition());
        telemetry.addData("Status", "Initialized");
    }

    @Override
    public void loop() {

        ButtonPress.giveMeInputsForOne(gamepad1.a, gamepad1.b, gamepad1.x, gamepad1.y,
                gamepad1.dpad_up, gamepad1.dpad_down, gamepad1.dpad_right, gamepad1.dpad_left,
                gamepad1.right_bumper, gamepad1.left_bumper,
                gamepad1.left_stick_button, gamepad1.right_stick_button,
                gamepad1.left_trigger > 0.5, gamepad1.right_trigger > 0.8
        );

        // Superstructure
        intake.setIntakeSingle(ButtonPress.isGamepad1_left_bumper_pressed(), gamepad1.right_bumper);
        shooter.setShooter(ButtonPress.isGamepad1_left_trigger_pressed());
        shooter.setTrigger(gamepad1.right_trigger > 0.8);

        wobbleGoalGrabber.setClaw(ButtonPress.isGamepad1_b_pressed());
        wobbleGoalGrabber.setWobbleRingVelocity(gamepad1.y, gamepad1.a);

        if (gamepad1.dpad_up) driveTrain.resetOdometer();
        if (gamepad1.left_stick_button) {
            auto.goTurnAngleFor(180);
        } else if (gamepad1.right_stick_button) {
            auto.goTurnAngleFor(0);
            //auto.goDirectionWithPoseTo(0, 0, 0);
        } else if (gamepad1.dpad_left | gamepad1.dpad_right) {
            driveTrain.driveStrafe(gamepad1.dpad_left, gamepad1.dpad_right, shooter.getShooterVelocity() > 1500);
        } else {
            auto.setIdle();
            driveTrain.driveMecanum(gamepad1.left_stick_x, gamepad1.right_stick_x * 0.8, -gamepad1.left_stick_y ,
                    gamepad1.x, false);
        }

        telemetry.addData("Arm Position", wobbleGoalGrabber.getArmPosition());
        telemetry.addData("Shooter Velocity", shooter.getShooterVelocity());
        telemetry.addData("Field Relative", driveTrain.getIsFieldRelative());
        telemetry.addData("Robot Pose", driveTrain.printPose());
        telemetry.addData("Status", "Run Time: " + runtime.toString());

        telemetry.update();
    }

    @Override
    public void stop() {
        //driveTrain.resetOdometer();
        driveTrain.resetEncoder();
        wobbleGoalGrabber.resetArm();
    }


}
