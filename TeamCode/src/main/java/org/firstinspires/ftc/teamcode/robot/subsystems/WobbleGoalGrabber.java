package org.firstinspires.ftc.teamcode.robot.subsystems;

import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorEx;
import com.qualcomm.robotcore.hardware.DcMotorSimple;
import com.qualcomm.robotcore.hardware.DigitalChannel;
import com.qualcomm.robotcore.hardware.Servo;
import com.qualcomm.robotcore.util.ElapsedTime;

import org.firstinspires.ftc.teamcode.lib.debugPacket.util.Range;
import org.firstinspires.ftc.teamcode.robot.RobotMap;

public class WobbleGoalGrabber {
    private final DcMotorEx ringArm = RobotMap.ringArm;
    private final Servo claw = RobotMap.claw;
    private static boolean isOpen = false;

    private static double clawPosition = 0.2; //default value

    private ElapsedTime clawDelay = new ElapsedTime();

    private static final double armPower = 0.5; // Test value

    private DigitalChannel armSwitch = RobotMap.armSwitch;

    public double getArmPosition() {
        return ringArm.getCurrentPosition();
    }

    private enum GrabberState {
        IDLE, PICK, HOLD, DUMP
    }

    public static void setGrabberState(GrabberState grabberState) {
        WobbleGoalGrabber.grabberState = grabberState;
    }

    private static GrabberState grabberState = GrabberState.IDLE; // Default State.

    public WobbleGoalGrabber() {
        ringArm.setDirection(DcMotorSimple.Direction.FORWARD);
        ringArm.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);

        ringArm.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        ringArm.setVelocityPIDFCoefficients(18, 0.5, 2, 10);
        claw.setDirection(Servo.Direction.REVERSE);
    }

    public void runWihoutEncoder () {
        ringArm.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
    }

    public GrabberState getGrabberState () {
        return grabberState;
    }

    public void setArmOpenLoop(boolean isBtnPressed, boolean isRevBtnPressed) {
        if (isBtnPressed)
            ringArm.setPower(armPower);//intake power
        else if (isRevBtnPressed)
            ringArm.setPower(-armPower);
        else
            ringArm.setPower(0);//stop intake
    }

    public void setArmPositionRaw (double position) {
        position = Range.clip(position, -2600, 200);
        double kP = 0.3;
        double error = getArmPosition() - position;
        double output = error * kP;
        if (Math.abs(error) < 250) {
            setArmVelocityRaw(output);
        } else {
            setArmVelocityRaw(0);
        }
    }

    public void setArmPosition (int position) {
        //position = Range.clip(position, -2600, 200);
//        ringArm.setTargetPosition(position);
//        ringArm.setPower(-0.4);
//        ringArm.setTargetPositionTolerance(200);
//        ringArm.setMode(DcMotor.RunMode.RUN_TO_POSITION);
    }


    public void setWobbleRing (boolean move, boolean reverse) {
        double power = 0;
        if (move) {
            power = 0.5;
        } else if (reverse) {
            power = -0.5;
        } else {
            power = 0;
        }
        ringArm.setPower(power);
    }

    public void setWobbleRingVelocity (boolean move, boolean reverse) {
        double velocity = 0;
        if (move) {
            velocity = 350;
        } else if (reverse) {
            velocity = -150;
        } else {
            velocity = 0;
        }
        setArmVelocityRaw(velocity);
    }

    public void setArmVelocityRaw (double input) {
        ringArm.setVelocity(input);
    }

    public void setClaw (boolean cmd) {
        double servoPos = 0.5;
        if (cmd) {
            isOpen = !isOpen;
        }
        if (isOpen) servoPos = 0.2;
        else servoPos = 0.95;
        claw.setPosition(servoPos);
    }

    public void setClawRaw (boolean open) {
        double power = open ? 0.25 : 0.95;
        claw.setPosition(power);
    }

    public void updateGrabber (boolean btnCtrl, GrabberState inputState) {

        GrabberState wantedState = inputState;

        switch (grabberState) {
            case IDLE:
                wantedState = btnCtrl ? GrabberState.PICK : GrabberState.IDLE;
                break;
            case PICK:
                wantedState = handlePickTransition(btnCtrl);
                break;
            case HOLD:
                wantedState = handleHoldTransition(btnCtrl);
                break;
            case DUMP:
                wantedState = btnCtrl ? GrabberState.IDLE : GrabberState.DUMP;
                break;
        }

        switch (wantedState) {
            case IDLE:
                idle();
                break;
            case PICK:
                pick();
                break;
            case HOLD:
                hold();
                break;
            case DUMP:
                dump();
                break;
        }

    }

    private GrabberState handlePickTransition(boolean btnCtrl) {
        if (getArmPosition() > -800) return GrabberState.HOLD;
        if (btnCtrl) return GrabberState.HOLD;
        return GrabberState.PICK;
    }

    private GrabberState handleHoldTransition(boolean btnCtrl) {
        if (getArmPosition() > -200) return GrabberState.PICK;
        if (btnCtrl) return GrabberState.DUMP;
        return GrabberState.HOLD;
    }

    private void idle() {
        setArmPosition(0);
        setClaw(false);
        if (!getArmSwitch())
            resetArm();
    }

    private void pick() {
        setArmPosition(-2400);
        setClaw(true);
    }

    private void hold() {
        clawDelay.reset();
        setClaw(false);
        if (clawDelay.milliseconds() < 200)
            setArmPosition(-800);
    }

    private void dump() {
        setArmPosition(-1800);
        if (getArmPosition() < -1600)
            setClaw(true);
    }

    public double getClawPos () {
        return claw.getPosition();
    }

    public void resetArm () {
        ringArm.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        ringArm.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
    }

    public boolean getArmSwitch () {
        return armSwitch.getState();
    }

}
