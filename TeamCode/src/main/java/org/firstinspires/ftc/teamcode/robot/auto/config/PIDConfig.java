package org.firstinspires.ftc.teamcode.robot.auto.config;

public class PIDConfig {

    private double kP = 0;
    private double kI = 0;
    private double kD = 0;
    private double kF = 0;
    private double integralZone = 0;
    public double error = 0;
    public double prevError = 0;
    public double prevTimeStamp = 0;
    public double errorSum = 0;
    public double errorRate = 0;
    public double output = 0;
    public boolean isIntegralUsed = false;
    public boolean isControlDone = false;

    public PIDConfig () {

    }

    public PIDConfig (double kP, double kI, double kD) {
        this.kP = kP;
        this.kI = kI;
        this.kD = kD;
    }

    public PIDConfig (double kP, double kI, double kD, double integralZone) {
        this.kP = kP;
        this.kI = kI;
        this.kD = kD;
    }

    public double getKP() {
        return kP;
    }

    public double getKI() {
        return kI;
    }

    public double getKD() {
        return kD;
    }

    public double getIntegralZone() {
        return integralZone;
    }

}
