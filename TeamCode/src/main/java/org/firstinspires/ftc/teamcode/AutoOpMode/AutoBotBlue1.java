package org.firstinspires.ftc.teamcode.AutoOpMode;

import com.acmerobotics.roadrunner.geometry.Pose2d;
import com.acmerobotics.roadrunner.geometry.Vector2d;
import com.acmerobotics.roadrunner.trajectory.Trajectory;
import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.util.ElapsedTime;

import org.firstinspires.ftc.teamcode.drive.SampleMecanumDrive;
import org.firstinspires.ftc.teamcode.robot.RobotMap;
import org.firstinspires.ftc.teamcode.robot.subsystems.Intake;
import org.firstinspires.ftc.teamcode.robot.subsystems.Shooter;
import org.firstinspires.ftc.teamcode.robot.subsystems.WobbleGoalGrabber;


@Autonomous(name="AutoBlue1", group="Auto")
//@Disabled
public class AutoBotBlue1 extends LinearOpMode {

    private final ElapsedTime runtime = new ElapsedTime();
    private Intake intake;
    private Shooter shooter;
    private WobbleGoalGrabber wobbleGoalGrabber;

    private RobotMap robotMap = new RobotMap();

    private static int pathNum = 4;

    @Override
    public void runOpMode() {
        robotMap.robotInit(hardwareMap);
        SampleMecanumDrive drive = new SampleMecanumDrive(hardwareMap);

        intake = new Intake();
        shooter = new Shooter();
        Shooter.setIsShooting(false);
        wobbleGoalGrabber = new WobbleGoalGrabber();
        wobbleGoalGrabber.runWihoutEncoder();
        wobbleGoalGrabber.setClaw(false);


        //drive.setPoseEstimate(new Pose2d(0, 0, Math.PI));
        waitForStart();
        pathNum = robotMap.getRingNumber();
        wobbleGoalGrabber.setClawRaw(false);

        Trajectory trajectory1 = drive.trajectoryBuilder(new Pose2d())
                .splineTo(new Vector2d(52, -18), 0)
                .addDisplacementMarker(60, () -> {
                    // This marker runs 20 inches into the trajectory
                    shooter.setShooter(true);
                    // Run your action in here!
                })
                .build();

        drive.followTrajectory(trajectory1);

        Pose2d pose2dTemp = new Pose2d(trajectory1.end().getX(), trajectory1.end().getY(), Math.toRadians(180));
        drive.turn(Math.toRadians(180));
        //pose2dTemp = drive.getPoseEstimate();
        sleep(500);

        shooter.setTrigger(true);
        sleep(300);
        shooter.setTrigger(false);
        for (int i = 1; i < 3; i++) {
            sleep(300);
            trigger();

        }
        shooter.setTrigger(false);
        shooter.setShooter(true);

        pose2dTemp = drive.getPoseEstimate();


        Trajectory trajectory6 = drive.trajectoryBuilder(pose2dTemp)
                .strafeRight(24)
                .addDisplacementMarker(60, () -> {
                    // This marker runs 20 inches into the trajectory
                    //shooter.setShooter(true);
                    // Run your action in here!
                })
                .build();

        pose2dTemp = drive.getPoseEstimate();

        Trajectory trajectory7 = drive.trajectoryBuilder(pose2dTemp)
                .back(20)
                .addDisplacementMarker(60, () -> {
                    // This marker runs 20 inches into the trajectory
                    //shooter.setShooter(true);
                    // Run your action in here!
                })
                .build();

        drive.followTrajectory(trajectory6);
        drive.followTrajectory(trajectory7);

    }

    public void trigger () {
        shooter.setTrigger(true);
        sleep(500);
        shooter.setTrigger(false);
        sleep(250);
    }

    public void wobble () {
        wobbleGoalGrabber.setClawRaw(false);
        wobbleGoalGrabber.setWobbleRing(true, false);
        sleep(1500);
        wobbleGoalGrabber.setWobbleRing(false, false);
        wobbleGoalGrabber.setClawRaw(true);
        sleep(1200);
        wobbleGoalGrabber.setWobbleRing(false, true);
        sleep(1325);
        wobbleGoalGrabber.setWobbleRing(false, false);
        wobbleGoalGrabber.setClawRaw(false);
    }
}
