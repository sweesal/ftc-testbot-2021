package org.firstinspires.ftc.teamcode.robot.subsystems;

import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorSimple;
import com.qualcomm.robotcore.util.Range;

import org.firstinspires.ftc.teamcode.robot.RobotMap;

public class Intake {

    private final DcMotor intake1 = RobotMap.intake1;
    private final DcMotor intake2 = RobotMap.intake2;

    public static void setIsIntakeTriggered(boolean isIntakeTriggered) {
        Intake.isIntakeTriggered = isIntakeTriggered;
    }

    private static boolean isIntakeTriggered = false;


    private enum IntakeState {
        INTAKE, REVERSE, IDLE;
    }

    private static final double intakePower = 0.9;

    public Intake() {
        intake1.setDirection(DcMotorSimple.Direction.REVERSE);
        intake2.setDirection(DcMotorSimple.Direction.REVERSE);
    }

    public void setIntakeFree (boolean isBtnPressed, double setPower) {
        if (isBtnPressed) {
            intake1.setPower(Range.clip(setPower, -0.99, 0.99));
        } else {
            intake1.setPower(0);
        }
    }

    public void setIntakeWithShooter (boolean isTriggered) {
        double intakePower = 0;
        if (isTriggered)
            isIntakeTriggered = !isIntakeTriggered;
    }

    public void setIntakeSingle(boolean isBtnPressed, boolean isRevBtnPressed) {
        double intakePower = 0;
        if (isBtnPressed)
            isIntakeTriggered = !isIntakeTriggered;
        if (isIntakeTriggered)
            intakePower = -0.9;
        if (isRevBtnPressed) {
            intakePower = 0.9;
            isIntakeTriggered = false;
        }
        intake1.setPower(intakePower);//stop intake
        intake2.setPower(intakePower);
    }

    public void setIntakeDual(boolean isBtnPressed, boolean isRevBtnPressed) {
        double intakePower = 0;
        if (isBtnPressed)
            intakePower = 0.9;
        if (isRevBtnPressed) {
            intakePower = -0.9;
        }
        intake1.setPower(intakePower);//stop intake
    }

}
