package org.firstinspires.ftc.teamcode.AutoOpMode;

import com.acmerobotics.roadrunner.geometry.Pose2d;
import com.acmerobotics.roadrunner.geometry.Vector2d;
import com.acmerobotics.roadrunner.trajectory.Trajectory;
import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.util.ElapsedTime;

import org.firstinspires.ftc.teamcode.drive.SampleMecanumDrive;
import org.firstinspires.ftc.teamcode.robot.RobotMap;
import org.firstinspires.ftc.teamcode.robot.subsystems.Intake;
import org.firstinspires.ftc.teamcode.robot.subsystems.Shooter;
import org.firstinspires.ftc.teamcode.robot.subsystems.WobbleGoalGrabber;


@Autonomous(name="AutoBlue", group="Auto")
//@Disabled
public class AutoBotBlue extends LinearOpMode {

    private final ElapsedTime runtime = new ElapsedTime();
    private Intake intake;
    private Shooter shooter;
    private WobbleGoalGrabber wobbleGoalGrabber;

    private RobotMap robotMap = new RobotMap();

    private static int pathNum = 4;

    @Override
    public void runOpMode() {
        robotMap.robotInit(hardwareMap);
        SampleMecanumDrive drive = new SampleMecanumDrive(hardwareMap);

        intake = new Intake();
        shooter = new Shooter();
        Shooter.setIsShooting(false);
        wobbleGoalGrabber = new WobbleGoalGrabber();
        wobbleGoalGrabber.runWihoutEncoder();
        wobbleGoalGrabber.setClaw(false);

        //drive.setPoseEstimate(new Pose2d(0, 0, Math.PI));

        Trajectory trajectory1 = drive.trajectoryBuilder(new Pose2d())
                .splineTo(new Vector2d(60, -34), 0)
                .addDisplacementMarker(60, () -> {
                    // This marker runs 20 inches into the trajectory
                    shooter.setShooter(true);
                    // Run your action in here!
                })
                .build();

        Pose2d pose2dTemp = new Pose2d(trajectory1.end().getX(), trajectory1.end().getY(), Math.toRadians(180));

        waitForStart();
        pathNum = robotMap.getRingNumber();
        wobbleGoalGrabber.setClawRaw(false);
        sleep(500);
        drive.followTrajectory(trajectory1);
        sleep(200);
        drive.turn(Math.toRadians(180));
        //pose2dTemp = drive.getPoseEstimate();
        sleep(500);

        shooter.setTrigger(true);
        sleep(300);
        shooter.setTrigger(false);
        for (int i = 1; i < 3; i++) {

            Trajectory trajectory2 = drive.trajectoryBuilder(pose2dTemp)
                    .strafeLeft(-8)
                    .build();
            drive.followTrajectory(trajectory2);
            sleep(300);
            trigger();
            pose2dTemp = drive.getPoseEstimate();
        }
        shooter.setTrigger(false);
        shooter.setShooter(true);

        Trajectory trajectory3 = drive.trajectoryBuilder(pose2dTemp)
                .lineToLinearHeading(new Pose2d(120, 15, 0))
                .addDisplacementMarker(60, () -> {
                    // This marker runs 20 inches into the trajectory
                    //shooter.setShooter(true);
                    // Run your action in here!
                })
                .build();

        //drive.followTrajectory(trajectory3);

        Trajectory trajectory4 = drive.trajectoryBuilder(pose2dTemp)
                .lineToLinearHeading(new Pose2d(95, -10, 0))
                .addDisplacementMarker(60, () -> {
                    // This marker runs 20 inches into the trajectory
                    //shooter.setShooter(true);
                    // Run your action in here!
                })
                .build();

        Trajectory trajectory5 = drive.trajectoryBuilder(pose2dTemp)
                .lineToLinearHeading(new Pose2d(70, 15, 0))
                .addDisplacementMarker(60, () -> {
                    // This marker runs 20 inches into the trajectory
                    //shooter.setShooter(true);
                    // Run your action in here!
                })
                .build();

        sleep(500);

        if (pathNum == 0)
            drive.followTrajectory(trajectory5);
        else if (pathNum == 1)
            drive.followTrajectory(trajectory4);
        else
            drive.followTrajectory(trajectory3);

        sleep(750);
        wobble();
        sleep(500);

        Trajectory trajectory6 = drive.trajectoryBuilder(drive.getPoseEstimate())
                .lineTo(new Vector2d(75, -30))
                .addDisplacementMarker(60, () -> {
                    // This marker runs 20 inches into the trajectory
                    //shooter.setShooter(true);
                    // Run your action in here!
                })
                .build();

        drive.followTrajectory(trajectory6);




    }

    public void trigger () {
        shooter.setTrigger(true);
        sleep(500);
        shooter.setTrigger(false);
        sleep(250);
    }

    public void wobble () {
        wobbleGoalGrabber.setClawRaw(false);
        wobbleGoalGrabber.setWobbleRing(true, false);
        sleep(1500);
        wobbleGoalGrabber.setWobbleRing(false, false);
        wobbleGoalGrabber.setClawRaw(true);
        sleep(1200);
        wobbleGoalGrabber.setWobbleRing(false, true);
        sleep(1325);
        wobbleGoalGrabber.setWobbleRing(false, false);
        wobbleGoalGrabber.setClawRaw(false);
    }
}
