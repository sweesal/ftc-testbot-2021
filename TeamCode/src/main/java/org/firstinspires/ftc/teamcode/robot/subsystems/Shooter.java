package org.firstinspires.ftc.teamcode.robot.subsystems;

import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorEx;
import com.qualcomm.robotcore.hardware.DcMotorSimple;
import com.qualcomm.robotcore.hardware.Servo;
import com.qualcomm.robotcore.util.ElapsedTime;
import com.qualcomm.robotcore.util.Range;

import org.firstinspires.ftc.teamcode.robot.RobotMap;

public class Shooter {

    private static final double MAX_SPEED = 1650;
    private final DcMotorEx shooter = RobotMap.shooter;
    private final Servo trigger = RobotMap.trigger;

    public static void setIsShooting(boolean isShooting) {
        Shooter.isShooting = isShooting;
    }

    private static boolean isShooting = false; //default state;
    private double triggerNum = 0;
    private double btnNum = 0;

    public double getRingLoad() {
        return ringLoad;
    }

    public void setRingLoad(double ringLoad) {
        this.ringLoad = ringLoad;
    }

    private double ringLoad = 0;
    private boolean isEmpty = true;
    private ElapsedTime countTimer = new ElapsedTime();

    private boolean isShoot = false;
    private static boolean isTriggered = false;

//    public static SHOOTER_STATE getState() {
//        return state;
//    }
//    private static SHOOTER_STATE state = SHOOTER_STATE.IDLE;

    public Shooter() {
        shooter.setDirection(DcMotorSimple.Direction.FORWARD);
        shooter.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.FLOAT);
        shooter.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
//        shooter.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
//        shooter.setVelocityPIDFCoefficients(16, 0.6 ,3, 10);
        trigger.setDirection(Servo.Direction.FORWARD);
    }


//    public enum SHOOTER_STATE {
//        IDLE, SPEEDING_UP, FULL_POWER, COSTING;
//    }
//
//    public void update (boolean btnCtrl, boolean stop, boolean isTriggered) {
//
//        switch (state) {
//            case IDLE:
//                handleIdleTransition(btnCtrl);
//                if (btnCtrl) {
//                    ringLoad = 3;
//                    isEmpty = false;
//                }
//                break;
//            case SPEEDING_UP:
//                handleSpeedingUpTransition(stop);
//                break;
//            case FULL_POWER:
//                 handleFullPowerTransition(isEmpty, stop);
//                 break;
//            case COSTING:
//                handleCoastingTransition();
//                break;
//        }
//
//
//
//        switch (state) {
//            case IDLE:
//            case COSTING:
//                setShooter(0);
//                break;
//            case SPEEDING_UP:
//                setShooter(0.99);
//                //setShooterVelocity(MAX_SPEED);
//                break;
//            case FULL_POWER:
//                setShooter(0.99);
//                //setShooterVelocity(MAX_SPEED);
//                if (isTriggered) {
//                    setTriggerFree(0.525);
//                    ringLoad -= ringLoad;
//                } else{
//                setTriggerFree(0.625);
//                }
//
//                break;
//        }
//
//    }
//
    private void setShooter(double power) {
        shooter.setPower(power);
    }
//
//    private void setShooterVelocity(double rpm) {
//        shooter.setVelocity(rpm);
//    }
//
//    private void handleIdleTransition(boolean isBtnPressed) {
//        if (isBtnPressed) {
//            state = SHOOTER_STATE.SPEEDING_UP;
//        }
////        if (getShooterVelocity() < 300) {
////            state = SHOOTER_STATE.IDLE;
////        } else {
////            state = SHOOTER_STATE.COSTING;
////        }
//    }
//
//    private void handleSpeedingUpTransition(boolean isStopRequested) {
//        if (isStopRequested) {
//            state = SHOOTER_STATE.COSTING;
//            return;
//        }
//        if (getShooterVelocity() >= MAX_SPEED) {
//            state = SHOOTER_STATE.FULL_POWER;
//            return;
//        }
//        state = SHOOTER_STATE.SPEEDING_UP;
//    }
//
//    private void handleFullPowerTransition(boolean isEmptyLoaded, boolean isStopRequested) {
//        if (isStopRequested | isEmptyLoaded) {
//            state = SHOOTER_STATE.COSTING;
//            return;
//        }
//        if (getShooterVelocity() < MAX_SPEED - 100) {
//            state = SHOOTER_STATE.SPEEDING_UP;
//        } else {
//            state = SHOOTER_STATE.FULL_POWER;
//        }
//    }
//
//    private void handleCoastingTransition() {
//        if (getShooterVelocity() < 100) {
//            state = SHOOTER_STATE.IDLE;
//        } else {
//            state = SHOOTER_STATE.COSTING;
//        }
//    }

    public void setShooter (boolean isBtnPressed) {
        int velocity = 0;
        if (isBtnPressed)
            isShooting = !isShooting;
        if (isShooting) {
            //velocity = (int)MAX_SPEED;
            setShooter(1);//shooting power
        } else {
            //velocity = 0;
            setShooter(0);//stop shooting
        }
        //shooter.setVelocity(velocity);
    }

    public void setShooterWithTune (boolean isBtnPressed) {
        int velocity = 0;
        if (isBtnPressed)
            isShooting = !isShooting;
        if (isShooting) {
            //velocity = (int)MAX_SPEED;
            setShooter(0.9);//shooting power
        } else {
            velocity = 0;
            setShooter(0);//stop shooting
        }
        //shooter.setVelocity(velocity);
    }

    public void setTriggerFree (double inputPos) {
        double pos = Range.clip(inputPos, -0.95, 0.95);
        trigger.setPosition(pos);
    }

    public void setTrigger (boolean isBtnPressed) {
        if (isBtnPressed)
            setTriggerFree(0.525);
        else
            setTriggerFree(0.625);
    }

//    public void countLoad (double currentVelocity) {
//        if (currentVelocity < MAX_SPEED - 100) {
//            countTimer.reset();
//        }
//        if (currentVelocity < MAX_SPEED - 100 && countTimer.milliseconds() < 10) {
//            ringLoad -= ringLoad;
//        }
//    }

    public double getShooterVelocity() {
        return shooter.getVelocity();
    }
}
