package org.firstinspires.ftc.teamcode.robot.auto.commands.commandBase;

public interface ICommand {

    void init ();

    void execute ();

    void end ();

    boolean isDone();

}
