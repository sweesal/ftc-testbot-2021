package org.firstinspires.ftc.teamcode.robot.auto.commands.commandBase;

import org.firstinspires.ftc.teamcode.robot.auto.utilities.AutonomousTimer;

public class SingleCommand implements ICommand{

    private AutonomousTimer timer = AutonomousTimer.getInstance();
    private double startTime;
    private double duration;

    @Override
    public void init() {

    }

    @Override
    public void execute() {

    }

    @Override
    public void end() {

    }

    @Override
    public boolean isDone() {
        return true;
    }

}
