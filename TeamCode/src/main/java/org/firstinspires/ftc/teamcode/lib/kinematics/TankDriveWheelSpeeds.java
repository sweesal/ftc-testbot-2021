package org.firstinspires.ftc.teamcode.lib.kinematics;

public class TankDriveWheelSpeeds {
    private final double leftSpeed;
    private final double rightSpeed;

    public TankDriveWheelSpeeds(double leftSpeed, double rightSpeed) {
        this.leftSpeed = leftSpeed;
        this.rightSpeed = rightSpeed;
    }

    public double getLeft() {
        return leftSpeed;
    }

    public double getRight() {
        return rightSpeed;
    }

    @Override
    public String toString() {
        return "L: " + leftSpeed + ", R: " + rightSpeed;
    }
}
