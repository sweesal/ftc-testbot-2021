package org.firstinspires.ftc.teamcode.lib.util;

import org.firstinspires.ftc.teamcode.lib.debugPacket.util.Range;

public class ButtonInput {
    public static double getSquaredOutput (double input) {
        return -Range.clip(input * input * Math.signum(input), -1, 1);
    }
}
